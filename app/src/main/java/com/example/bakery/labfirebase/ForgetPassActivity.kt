package com.example.bakery.labfirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forget_pass.*

class ForgetPassActivity : AppCompatActivity() {

    lateinit var mAuth:FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_pass)

        mAuth = FirebaseAuth.getInstance()

        btnSubmit.setOnClickListener{
            sendEmail()
        }
    }

    private fun sendEmail() {
        val email:String = etEmail.text.toString().trim()

        if (email.isEmpty()) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show()
        } else {
            forGetPassword(email)
        }



    }

    private fun forGetPassword(email: String) {

        mAuth.sendPasswordResetEmail(email).addOnCompleteListener { task: Task<Void> ->

            if (task.isComplete) {
                val loginIntent = Intent(this,MainActivity::class.java)
                startActivity(loginIntent)

                Toast.makeText(this,"Please check your inbox Reset Password and Login",Toast.LENGTH_SHORT).show()
            }

        }
    }

}
