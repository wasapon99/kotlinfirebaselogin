package com.example.bakery.labfirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    val mAuth = FirebaseAuth.getInstance()
    lateinit var mDatabase:DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mDatabase = FirebaseDatabase.getInstance().getReference("Users")

        btnRegister.setOnClickListener {
            register()
        }
    }

    private fun register() {
        val email: String = tvEmail.text.toString()
        val password: String = tvPassword.text.toString()
        val name: String = tvName.text.toString()

        if (!name.isEmpty() && !password.isEmpty() && !name.isEmpty()) {
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = mAuth.currentUser
                    val uId = user!!.uid
                    mDatabase.child(uId).child("Name").setValue(name)
                    mDatabase.child(uId).child("Email").setValue(email)
                    Toast.makeText(this,"Success",Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this,InfoActivity::class.java))
                } else {
                    Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show()

                }
            }
        } else {
            Toast.makeText(this,"Insert",Toast.LENGTH_SHORT).show()
        }
    }
}
