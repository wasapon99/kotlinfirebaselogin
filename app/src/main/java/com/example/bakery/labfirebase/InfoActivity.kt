package com.example.bakery.labfirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_info.*
import com.google.firebase.database.DatabaseReference



class InfoActivity : AppCompatActivity() {

    val mAuth = FirebaseAuth.getInstance()
    lateinit var mDatabase :DatabaseReference
    var user = FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        mDatabase = FirebaseDatabase.getInstance().getReference("Users")

        val uid = user!!.uid

        btnSingOut.setOnClickListener {
            singOut()
        }

        mDatabase.child(uid).child("Name").addValueEventListener( object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val name:String = snapshot.getValue(String()::class.java)!!
                tvDisplay.text =  "Welcome: $name"
            }
        })
    }

    private fun singOut() {
        mAuth.signOut()
        startActivity(Intent(this, MainActivity::class.java))
        Toast.makeText(this,"Logged Out",Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        singOut()
    }

}
