package com.example.bakery.labfirebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener


class MainActivity : AppCompatActivity() {
    val mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            login()
        }

        btnRegister.setOnClickListener {
            register()
        }

        tvForgetPass.setOnClickListener {
            forgetPass()
        }

    }

    private fun forgetPass() {
        startActivity(Intent(this,ForgetPassActivity::class.java))
        finish()
    }

    private fun register() {
        startActivity(Intent(this,RegisterActivity::class.java))

    }

    private fun login() {
        val email:String = tvEmail.text.toString()
        val password:String = tvPassword.text.toString()

        if (!email.isEmpty() && !password.isEmpty()) {
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, OnCompleteListener { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, InfoActivity::class.java))
                    Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            Toast.makeText(this,"Please Insert",Toast.LENGTH_SHORT).show()
        }
    }

}



